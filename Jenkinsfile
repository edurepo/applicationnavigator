pipeline {
    agent any

    environment{
        downloadRootUrl = "https://esmsvc.ellucian.com:8443"
        esmVersion = "1.11"
        productID = "BXE_APPNAV"
        rlsversion = "3.0"
        filepath = "MAIN/BA GENERAL/9X/BANNER APPLICATION NAVIGATOR/9X-RELEASES/"
        filename = "application-navigator-30000u.trz"
        file="application-navigator-30000u"
        appname="applicationNavigator"
    }

    stages {
        
        stage('Download App from Ellucian') {
            
            steps {
                echo 'Downloading app...'
                sh 'mkdir temp'
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'ESM', usernameVariable: 'esmuser', passwordVariable: 'esmpass']]){
                    sh 'curl -u $esmuser:$esmpass -G "$downloadRootUrl/releaseservices/Release" \
                        --data-urlencode "login=$esmuser" \
                        --data-urlencode "productid=$productID" \
                        --data-urlencode "rlsversion=$rlsversion" \
                        --data-urlencode "filepath=$filepath" \
                        --data-urlencode "filename=$filename" \
                        -o temp/$filename'
                }
                dir ('temp'){
                    sh 'tar xvzf $filename'
                }
            }
        }
        stage('Extract App') {
            steps {
                echo 'Extracting App...'
                sh 'unzip temp/$file/java/release-$appname-$rlsversion.zip -d temp/release'
                sh 'mkdir $appname'
                sh 'unzip temp/release/webapp/$appname-$rlsversion.war -d $appname'
            }
        }
        stage('Configure App') {
            steps {
                echo 'Configure App....'
                sh 'cp -r WEB-INF $appname/'
                sh 'if [ -e css/*.css ]; then cp css/*.css $appname/css/;fi;'
                
            }
        }
        
        stage('Build Container'){
            steps{
                echo 'Building Container...'
                withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'docker-registry', usernameVariable: 'DOCKER_REGISTRY_URL', passwordVariable: 'IMAGE_NAME']]){
                    script {
                        docker.withRegistry("${DOCKER_REGISTRY_URL}", 'docker-registry-credentials') {
                            def img = docker.build("${IMAGE_NAME}/applicationnavigator:$rlsversion")
                            img.push()
                        }
                    }
                }
            }   
        }
        
    }
    post {
        always {
            cleanWs()
        }
    }
}
