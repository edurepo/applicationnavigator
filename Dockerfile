FROM edurepo/banner9-selfservice:tomcat8.5-jre8-alpine

USER root

# Fix timezone
ENV TIMEZONE=America/Denver
RUN cp -f /usr/share/zoneinfo/$TIMEZONE /etc/localtime
RUN echo $TIMEZONE > /etc/timezone

ENV APPNAME applicationNavigator

# Healthchecks
RUN mkdir -p /usr/local/tomcat/webapps/ROOT \
 && echo -n "pong" > /usr/local/tomcat/webapps/ROOT/ping \
 && chown -R tomcat:tomcat /usr/local/tomcat/webapps/ROOT
COPY ./alive.sh /usr/bin/alive
COPY ./ready.sh /usr/bin/ready
HEALTHCHECK --interval=30s --timeout=10s --start-period=3m --retries=3 CMD [ "sh", "-c", "alive && ready" ]

USER tomcat

COPY --chown=tomcat:tomcat applicationNavigator /usr/local/tomcat/webapps/${APPNAME}