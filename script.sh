#!/bin/bash

#ESMUSER
#ESMPASS

DOWNLOADROOTURL="https://esmsvc.ellucian.com:8443"
ESMVERSION="1.11"
PRODUCTID="BXE_APPNAV"
RLSVERSION="3.0"
RLSARTIFACTSPATH="MAIN/BA%20GENERAL/9X/BANNER%20APPLICATION%20NAVIGATOR/9X-RELEASES/"
RLSARTIFACTSFILE="application-navigator-30000u.trz"
RLSARTIFACTFILENAME="application-navigator-30000u"
APPNAME="applicationNavigator"


mkdir temp


#Download installer
echo "Download source files"
curl -u $ESMUSER:$ESMPASS -X GET "$DOWNLOADROOTURL/releaseservices/Release?login=$ESMUSER&productid=$PRODUCTID&rlsversion=$RLSVERSION&filepath=$RLSARTIFACTSPATH&filename=$RLSARTIFACTSFILE" -o temp/$RLSARTIFACTSFILE


#Uncompress source files
echo "Uncompress source files"
cd temp
tar xvzf $RLSARTIFACTSFILE
cd ..

#Uncompress release zip
unzip temp/$RLSARTIFACTFILENAME/java/release-$APPNAME-$RLSVERSION.zip -d temp/release

#Uncompress app
mkdir $APPNAME
unzip temp/release/webapp/$APPNAME-$RLSVERSION.war -d $APPNAME


#Copy config into app
cp -r WEB-INF $APPNAME/
if [ -e css/*.css ]
then
    cp css/* $APPNAME/css/
fi

#Remove temp
#rm -rf temp
