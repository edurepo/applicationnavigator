#!/bin/bash

HEALTHCHECK_URL="http://localhost:8080/ping"
HEALTHCHECK_CONTENT="pong"

# Make ./info if it doesn't exist
[ -d info ] || mkdir "info";

if [ -f "/usr/local/tomcat/info/started" ]; then
    # Alive?
    wget -q -O - "${HEALTHCHECK_URL}" | grep -qE "${HEALTHCHECK_CONTENT}";
    exit $?;
elif [ -f "/usr/local/tomcat/info/dead" ]; then
    # Dead!
    exit -1;
elif grep -qE "INFO\s\\[main\]\sorg\.apache\.catalina\.startup\.Catalina\.start\sServer\sstartup\sin\s\d+\sms" "logs/catalina.log"; then
    # Start up finished
    
    # Any exceptions?
    SEVERE_EXCEPTIONS=$(grep -c "\sSEVERE\s\[" logs/catalina.log)
    
    if [ "${SEVERE_EXCEPTIONS}" -eq "0" ]; then 
        # Startup succeeded
        touch "/usr/local/tomcat/info/started";
        exec "$0";
    else
        # Startup failed
        touch "/usr/local/tomcat/info/dead";
        exec "$0";
    fi
else
    # Still starting up
    exit 0;
fi
