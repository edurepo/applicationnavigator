#!/bin/bash

HEALTHCHECK_URL="http://localhost:8080/$APPNAME"
HEALTHCHECK_CONTENT="url=seamless"

if [ -f "/usr/local/tomcat/info/started" ]; then
    # Ready?
    wget -q -O - "${HEALTHCHECK_URL}" | grep -qE "${HEALTHCHECK_CONTENT}";
    exit $?;
elif [ -f "/usr/local/tomcat/info/dead" ]; then
    # Dead!
    exit -1;
else
    # Starting
    exit 1;
fi
