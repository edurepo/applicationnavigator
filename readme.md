
# Applicaiton Navigator - Build pipeline

## Work in Process

jenkins plugins
- Pipeline Utility Steps
- 

Tools that need to be installed on jenksins 
- tar
- unzip
- docker

Credentials
ESM - 
docker-registry-credentials -


Parameters

banner_configuration.groovy
```
ONLINEHELP_URL - http://HOST:POST/banner9OH/
BANNER_TRANSACTIONTIMEOUT - 30
BANNER_PICTUREPATH - /opt/banner/images
BANNER8_SS_URL - http://<SSO MANAGER HOST>:<PORT>/ssomanager/c/SSB?pkg=
```


applicationNavigator_configuration.groovy
```
SSBENABLED - false
SSBORACLEUSERSPROXIED - false
GRAILS_PLUGIN_SPRINGSECURITY_LOGOUT_AFTERLOGOUTURL - http://APPLICATION_NAVIGATOR_HOST:PORT/applicationNavigator/logout/customLogout
GRAILS_PLUGIN_SPRINGSECURITY_HOMEPAGEURL - http://APPLICATION_NAVIGATOR_HOST:PORT/applicationNavigator
BANNER9_URL - http://BANNER9_HOST:PORT
CAS_URL - http://CAS_HOST:PORT/cas
SEAMLESS_BRANDTITLE - Ellucian University
SEAMLESS_SESSIONTIMEOUT - 30
SEAMLESS_SESSIONTIMEOUTNOTIFICATION - 5
BANNER_ANALYSTICS_TRACKERID - 
BANNER_ANALYSTICS_ALLOWELLUCIANTRACKER - true
```


web.xml

In the groovy file release/installer/groovy/net/hedtech/banner/installer/actions/CreateWar.groovy has the sections to insert the CAS config to the web.xml for the application.  Specifically the function `updateWebXmlCasConfiguration` which calls 4 additions functions for the data to insert `getCasFilters`, `getCasSaml11ProtocolFilters`, `getCasValidationFilterString` and `getCasFilterMappings`.